<?php
//$mycalc = new MyCalculator( 12, 6);
//echo $mycalc- > add(); // Displays 18
//echo $mycalc- > multiply(); // Displays 72

class MyCalculator{

    private $_val1 , $_val2;

    public function __construct($val1, $val2)
    {
        $this->_val1 = $val1;
        $this->_val2 = $val2;
    }

    public function add(){
        $addition = $this->_val1 + $this->_val2;
        return $addition;
    }

    public function sub(){
        $substract = $this->_val1 - $this->_val2;
        return $substract;
    }

    public function mult(){
        $multiply = $this->_val1 * $this->_val2;
        return $multiply;
    }

    public function div(){
        $division = $this->_val1 / $this->_val2;
        return $division;
    }


}
$mycalc = new MyCalculator( 12, 6);

echo "Addition is : " .$mycalc-> add()."<br>"; // Displays 18
echo "Substraction is : " .$mycalc-> sub()."<br>"; // Displays 18
echo "Multiplication is : " .$mycalc-> mult()."<br>"; // Displays 18
echo "divission is : " .$mycalc-> div()."<br>"; // Displays 18
