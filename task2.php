<?php

//Sample Dates : 1981-11-03, 2013-09-04
//Expected Result : Difference : 31 years, 10 months, 1 days

$Date1 = new DateTime("1981-11-03");
$Date2 = new DateTime("2013-09-04");

$difference = date_diff($Date1,$Date2);

echo "Difference is ".$difference->y. ' years '. $difference->m .' months ' .$difference->d .' Days ';

//Hints2:        diff() method
